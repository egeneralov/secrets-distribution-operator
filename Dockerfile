FROM python:3.7
RUN pip install kopf kubernetes requests pyinstaller
RUN pyinstaller -F $(which kopf)

FROM debian:buster-slim
COPY --from=0 /dist/kopf /bin/
ADD secrets-distribution-operator.py .
CMD kopf run secrets-distribution-operator.py

