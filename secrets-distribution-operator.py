import os
import kopf
import kubernetes
import yaml
import json


@kopf.on.create('', 'v1', 'namespaces')
def createSecretOnNamespaceCreation(meta, spec, namespace, logger, **kwargs):
  logger.info(
    f"started on_namespace_create_list_secrets_in_namespace: %s",
    meta["name"]
  )
  result = kubernetes.client.CoreV1Api().list_namespaced_secret(
    namespace = meta["name"]
  )
  secrets = [ i.metadata.name for i in result.items ]
  
  filename = os.environ.get("SECRET_FILE", "/opt/secret.yaml")
  with open(filename, "r") as f:
    secret = f.read()
  
  secret = yaml.load(
    secret,
    Loader = yaml.FullLoader
  )
  
  # if secret not exist in namespace
  if secret["metadata"]["name"] not in secrets:
    # remove fields
    try:
      secret.pop("apiVersion")
    except KeyError:
      pass
    try:
      secret["metadata"].pop("creationTimestamp")
    except KeyError:
      pass
    try:
      secret["metadata"].pop("resourceVersion")
    except KeyError:
      pass
    try:
      secret["metadata"].pop("uid")
    except KeyError:
      pass
    try:
      secret["metadata"].pop("selfLink")
    except KeyError:
      pass
    try:
      secret["metadata"]["namespace"] = meta["name"]
    except KeyError:
      pass
    
    kubernetes.client.CoreV1Api().create_namespaced_secret(
      namespace = meta["name"],
      body = kubernetes.client.V1Secret(**secret),
    )
    
    logger.info(
      f"wildcard secret created in namespace %s",
      meta["name"]
    )
  logger.info(
    f"finished on_namespace_create_list_secrets_in_namespace: %s",
    meta["name"]
  )
