# secrets-distribution-operator

Operator based on [kopf](https://github.com/zalando-incubator/kopf). Just distribute your secret (like docker-registry or tls) in cluster-wide.

## use

    mkdir -p .helm
    wget -O artifacts.zip https://gitlab.com/egeneralov/secrets-distribution-operator/-/jobs/artifacts/v1.0.0/download?job=helm
    unzip artifacts.zip
    tar xzvf secrets-distribution-operator-0.1.0.tgz -C .helm/
    rm artifacts.zip secrets-distribution-operator-0.1.0.tgz
    cp .helm/secrets-distribution-operator/values.yaml secrets-distribution-operator.yaml
    ${EDITOR:-nano} secrets-distribution-operator.yaml
    OPTS="secrets-distribution-operator .helm/secrets-distribution-operator --namespace secrets-distribution-operator -f secrets-distribution-operator.yaml --wait"
    helm install --name ${OPTS} || helm upgrade ${OPTS}
    
